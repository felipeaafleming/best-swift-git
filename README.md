# Best Swift Git

This project is just a lab for infinity scrolling on table view, using git repositories data.

## Getting Started

Based on Github API, the app purposal is to display an infinite scroll with all repositories, showing the repository name, stars, photo and author's name. It's included also a pull to refresh mechanism to update the list and also some unit tests to increase the code coverage. The project uses Viper Design Pattern and all views were created using code view (besides LaunchScreen). 

### Prerequisites

After cloning the repository, run pod install and open the .xcworkspace.

```
pod install
```
## Tests

Some tests are implemented for AppDelegate, RepositoriesListRouterTests and RepositoriesListPresenterTests. To run all the tests available, select the schema BestSwiftGitTests and hit Test button.

## Slather

To generate the code coverage report (after running the tests), use the following command at the same directory of .xcodeproj file:

```
fastlane ios slather_lane
```

_Note: For the current version of the project we got Test Coverage: 87.59% =)_

## Dependencies

* [PureLayout](https://github.com/PureLayout/PureLayout) - To create the views programmatically
* [ObjectMapper](https://github.com/tristanhimmelman/ObjectMapper), [Alamofire](https://github.com/Alamofire/Alamofire), [RxSwift](https://github.com/ReactiveX/RxSwift) and [AlamofireObjectMapper](https://github.com/tristanhimmelman/AlamofireObjectMapper) - Network and parsing objects
* [Kingfisher](https://github.com/onevcat/Kingfisher) - Used to download and cache images
* [SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD) - A beautiful spinning

## Authors

* **Felipe Fleming** - [Best Swift Git](https://bitbucket.org/felipeaafleming/best-swift-git/)

## License

This project is licensed under the MIT License
