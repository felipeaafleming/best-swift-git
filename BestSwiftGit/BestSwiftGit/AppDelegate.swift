//
//  AppDelegate.swift
//  BestSwiftGit
//
//  Created by Felipe Fleming on 11/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var repositoriesListViewController: UIViewController!
    
    override init() {
        super.init()
        repositoriesListViewController = RepositoriesListRouter.createModule()
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.rootViewController = repositoriesListViewController
        window!.makeKeyAndVisible()
        return true
    }

}

