//
//  Utils.swift
//  BestSwiftGit
//
//  Created by Felipe Fleming on 14/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import UIKit

struct Utils {
    struct Colors {
        static var black = #colorLiteral(red: 0.003920602147, green: 0.003922124859, blue: 0.003920506686, alpha: 1)
        static var lightGray = #colorLiteral(red: 0.9136260152, green: 0.9137827754, blue: 0.9136161804, alpha: 1)
        static var gray = #colorLiteral(red: 0.8077545762, green: 0.8078941703, blue: 0.8077457547, alpha: 1)
        static var blue = #colorLiteral(red: 0.5422741175, green: 0.7359521985, blue: 0.8404833674, alpha: 1)
        static var lightBlue = #colorLiteral(red: 0.6082084179, green: 0.8310840726, blue: 0.8902772069, alpha: 1)
        
    }
    
    struct Images {
        static var star = UIImage(named: "star")
        static var logo = UIImage(named: "logo")
    }
}
