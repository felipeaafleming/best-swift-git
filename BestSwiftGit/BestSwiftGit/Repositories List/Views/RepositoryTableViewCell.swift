//
//  RepositoryTableViewCell.swift
//  BestSwiftGit
//
//  Created by Felipe Fleming on 14/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import UIKit
import PureLayout
import Kingfisher

class RepositoryTableViewCell: UITableViewCell {
    
    var repository : RepositoryData? {
        didSet {
            if let url = URL(string: self.repository?.owner.avatar ?? "") {
                avatarImageView.kf.setImage(with: url)
            }
            self.nameLabel.text = self.repository?.name
            self.authorLabel.text = self.repository?.owner.login
            self.starsLabel.text = "\(self.repository?.stars ?? 0)"
        }
    }
    
    private var containerView: UIView = {
        let view = UIView(forAutoLayout: ())
        view.layer.cornerRadius = 5.0
        view.backgroundColor = Utils.Colors.lightGray
        return view
    }()
    
    private var avatarImageView: UIImageView = {
        let imageView = UIImageView(forAutoLayout: ())
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        imageView.autoSetDimensions(to: CGSize(width: 60, height: 60))
        return imageView
    }()
    
    private var nameLabel: UILabel = {
        let label = UILabel(forAutoLayout: ())
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = Utils.Colors.black
        return label
    }()
    
    private var authorLabel: UILabel = {
        let label = UILabel(forAutoLayout: ())
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Utils.Colors.black
        return label
    }()
    
    private var starImageView: UIImageView = {
        let imageView = UIImageView(forAutoLayout: ())
        imageView.autoSetDimensions(to: CGSize(width: 20, height: 20))
        imageView.image = Utils.Images.star?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        imageView.tintColor = UIColor.black
        return imageView
    }()
    
    private var starsLabel: UILabel = {
        let label = UILabel(forAutoLayout: ())
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Utils.Colors.black
        return label
    }()
    
    private var positionLabel: UILabel = {
        let label = UILabel(forAutoLayout: ())
        label.font = UIFont.systemFont(ofSize: 20)
        label.textColor = Utils.Colors.black
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = Utils.Colors.blue
        self.addSubview(containerView)
        containerView.addSubview(avatarImageView)
        containerView.addSubview(nameLabel)
        containerView.addSubview(authorLabel)
        containerView.addSubview(starImageView)
        containerView.addSubview(starsLabel)
        
        containerView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8))
        
        avatarImageView.autoPinEdge(.left, to: .left, of: containerView, withOffset: 16.0)
        avatarImageView.autoAlignAxis(.horizontal, toSameAxisOf: containerView)
        
        nameLabel.autoPinEdge(.left, to: .right, of: avatarImageView, withOffset: 8.0)
        nameLabel.autoPinEdge(.top, to: .top, of: avatarImageView)
        
        authorLabel.autoPinEdge(.left, to: .left, of: nameLabel)
        authorLabel.autoPinEdge(.top, to: .bottom, of: nameLabel, withOffset: 4.0)
        
        starImageView.autoPinEdge(.left, to: .left, of: authorLabel, withOffset: -2.0)
        
        starsLabel.autoPinEdge(.left, to: .right, of: starImageView)
        starsLabel.autoAlignAxis(.horizontal, toSameAxisOf: starImageView)
        starsLabel.autoPinEdge(.top, to: .bottom, of: authorLabel, withOffset: 4.0)
        
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
