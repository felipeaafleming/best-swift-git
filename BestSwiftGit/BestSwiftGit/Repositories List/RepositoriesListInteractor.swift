//
//  RepositoriesListInteractor.swift
//  BestSwiftGit
//
//  Created Felipe Fleming on 14/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit
import RxSwift

class RepositoriesListInteractor: RepositoriesListInteractorInputProtocol {

    weak var presenter: RepositoriesListInteractorOutputProtocol?
    var disposeBag = DisposeBag()
    
    func retrieveRepositories(for page: Int) {
        Service.search(page).subscribe(onNext: { (searchRepositoryData) in
            self.presenter?.didReceiveRepository(searchRepositoryData?.items)
        }, onError: { (error) in
            self.presenter?.didReceiveRepository(nil)
        }, onCompleted: {
            
        }).disposed(by: disposeBag)
    }
}
