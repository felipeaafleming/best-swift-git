//
//  RepositoriesListRouter.swift
//  BestSwiftGit
//
//  Created Felipe Fleming on 14/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class RepositoriesListRouter: RepositoriesListWireframeProtocol {

    weak var viewController: UIViewController?

    static func createModule() -> UIViewController {
        // Change to get view from storyboard if not using progammatic UI
        let view = RepositoriesListViewController()
        let interactor = RepositoriesListInteractor()
        let router = RepositoriesListRouter()
        let presenter = RepositoriesListPresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }
}
