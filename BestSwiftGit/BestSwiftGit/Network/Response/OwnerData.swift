
//
//  OwnerData.swift
//  BestSwiftGit
//
//  Created by Felipe Fleming on 14/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import ObjectMapper

class OwnerData: Mappable {
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        login <- map["login"]
        avatar <- map["avatar_url"]
    }
    
    var login: String!
    var avatar: String?
}
