//
//  SearchRepositoryData.swift
//  BestSwiftGit
//
//  Created by Felipe Fleming on 14/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import ObjectMapper

class SearchRepositoryData: Mappable {
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        items <- map["items"]
    }
    
    var items: [RepositoryData]!
}
