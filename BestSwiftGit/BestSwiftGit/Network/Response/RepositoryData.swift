//
//  RepositoryData.swift
//  BestSwiftGit
//
//  Created by Felipe Fleming on 14/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import ObjectMapper

class RepositoryData: Mappable {
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        stars <- map["stargazers_count"]
        owner <- map["owner"]
        
    }
    
    var name: String!
    var stars: Int!
    var owner: OwnerData!
}
