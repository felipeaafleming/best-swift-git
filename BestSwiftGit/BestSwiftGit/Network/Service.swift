//
//  Service.swift
//  BestSwiftGit
//
//  Created by Felipe Fleming on 14/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import AlamofireObjectMapper
import ObjectMapper

public class Service {
    
    static func search(_ page: Int)-> Observable<SearchRepositoryData?> {
        return Observable<SearchRepositoryData?>.create({ (observer) -> Disposable in
            print("page=\(page)")
            let request = Alamofire.request(Endpoints.Repositories.search.url + "&page=\(page)", method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: nil)
                .validate()
                .responseObject(completionHandler: { (response: DataResponse<SearchRepositoryData>) in
                    observer.onNext(response.value)
                })
            return Disposables.create(with: {
                request.cancel()
            })
        })
    }

}
