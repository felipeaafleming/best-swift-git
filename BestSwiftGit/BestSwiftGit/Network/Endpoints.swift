//
//  Endpoints.swift
//  BestSwiftGit
//
//  Created by Felipe Fleming on 14/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import AlamofireObjectMapper
import ObjectMapper

struct API {
    static var baseUrl = "https://api.github.com/"
}

protocol Endpoint {
    var path: String { get }
    var url: String { get }
}

enum Endpoints {
    enum Repositories: Endpoint {
        case search
        
        public var path: String {
            switch self {
            case .search:
                return "search/repositories?q=language:swift&sort=stars"
            }
        }
        
        public var url: String {
            switch self {
            case .search: return "\(API.baseUrl)\(path)"
            }
        }
    }
}
