//
//  RepositoriesListRouterTests.swift
//  BestSwiftGitTests
//
//  Created by Felipe Fleming on 15/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import XCTest

class RepositoriesListRouterTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testCreateViewController() {
        let viewController = RepositoriesListRouter.createModule()
        XCTAssertNotNil(viewController, "RepositoriesListViewController should be the one I set")
    }
}
