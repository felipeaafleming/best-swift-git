//
//  RepositoriesListPresenterTests.swift
//  BestSwiftGitTests
//
//  Created by Felipe Fleming on 15/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import XCTest

class RepositoriesListPresenterTests: XCTestCase {
    
    class FakeRepositoriesListPresenter: RepositoriesListPresenter {
        var receivedRepository: Bool = false
        
        override func didReceiveRepository(_ repositories: [RepositoryData]?) {
            if repositories != nil {
                receivedRepository = true
                RepositoriesListPresenterTests.receivedRepositoryExpectation.fulfill()
            }
        }
    }
    static let receivedRepositoryExpectation = XCTestExpectation(description: "Repositories successfully received")

    var view: RepositoriesListViewController!
    var interactor: RepositoriesListInteractor!
    var router: RepositoriesListRouter!
    var presenter:FakeRepositoriesListPresenter!
    
    override func setUp() {
        self.view = RepositoriesListViewController()
        self.interactor = RepositoriesListInteractor()
        self.router = RepositoriesListRouter()
        self.presenter = FakeRepositoriesListPresenter(interface: self.view, interactor: interactor, router: router)
        self.interactor.presenter = self.presenter
    }

    func testLoadRepository() {
        presenter.loadRepository()
        wait(for: [RepositoriesListPresenterTests.receivedRepositoryExpectation], timeout: 10.0)
        XCTAssertTrue(self.presenter.receivedRepository)
    }
}
