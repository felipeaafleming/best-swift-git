//
//  AppDelegateTests.swift
//  BestSwiftGitTests
//
//  Created by Felipe Fleming on 15/04/19.
//  Copyright © 2019 Fleming. All rights reserved.
//

import XCTest

class AppDelegateTests: XCTestCase {
    
    var appDelegate: AppDelegate = AppDelegate();
    var window: UIWindow? = UIWindow()
    
    override func setUp() {
        super.setUp()
        appDelegate.window = window
    }

    override func tearDown() {
        super.tearDown()
        window = nil
    }

    func testThatHasInitializerAfterInit() {
        XCTAssertNotNil(appDelegate.repositoriesListViewController, "App should have RepositoriesViewController after starting")
    }
    
    func testWindowIsKeyAfterApplicationLaunch() {
        _ = appDelegate.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        if let window = appDelegate.window {
            XCTAssertTrue(window.isKeyWindow)
        } else {
            XCTFail("App Delegate window should not be nil")
        }
    }
    
    func testThatDidFinishLaunchingWithOptionsReturnsTrue() {
        XCTAssertTrue(appDelegate.application(UIApplication.shared, didFinishLaunchingWithOptions: nil), "Should return true from didFinishLaunchingWithOptions")
    }
}
